# PHPWebAPI

A simple PHP web API framework, a light-weight alternative PHP MVC Framework for small projects.

## Requirements

 * PHP 5 or newer
 * Mod rewrite

## Setup

Create an index file with the following contents

    <?php
    define('APP_ROOT', __DIR__);
    define('APP_BASE_HREF', '/');
    require_once 'vendor/autoload.php';

    use \PhpWebApi\Router;

    $router = new Router();
    $router->bootstrap();

If you like to enable CORS, you may do so.

    // params: origin, headers, methods
    $router->allowCors('*', 'Authorization', 'DELETE, GET, POST, PUT')
        ->bootstrap();

Rewrite your URL in your `.htaccess` file to your index.
Note, if you deployed your application in a subfolder (ex: http://example.com/yourappname),
set the `RewriteBase` to your folder's name.

    ## Rewrite base folder
    RewriteBase /

    ## Rewrite all except assets folder
    RewriteCond %{REQUEST_URI} !^/assets
    RewriteRule . index.php [L]

Also set the `APP_BASE_HREF` to your folder's name.

    define('APP_BASE_HREF', '/');

## Creating controllers

Create a PHP file with your controller name and `Controller.php`. Ex: `HomeController.php`.
Create a class (you can set it as final) and name it your controller name and `Controller`.

    <?php
    final class HomeController extends \PhpWebApi\Controller {
        // functions here ...
    }

## Creating methods

The naming convention for your methods will be as follows: `<HTTP Method><Route name>Action()`.

    <?php
    // File: controllers/HomeController.php
    final class HomeController extends \PhpWebApi\Controller {
        // GET http://localhost/yourappbasename/ (This is home/index action)
        function getIndexAction() {
            self::text('Hello World');
        }
    }

    // File: controllers/HomeController.php
    final class ContactController extends \PhpWebApi\Controller {
        // POST http://localhost/yourappbasename/contact/submit
        function postSubmitAction() {
            self::text('Contact form POST received');
        }
    }


You can respond with the following: `badRequest`, `notFound`, `ok`, `unauthorized`, `serverError`.
You can also pass string or object as parameters. Objects will be encoded as JSON.

    <?php
    self::badRequest('Invalid parameters');

    self::ok('This is a text');

    // other ok responses
    self::json(['status' => 'test']);
    self::csv('filename.csv', [ [...], [...] ]);
    self::text('Simple Text');

## HTTP Parameters

### GET or POST

You can pass HTTP GET and POST parameters as `$_GET` and `$_POST` global variable accordingly.

    GET http://api.example.com/posts/search?query=Hello+World

In the example above, it calls the `PostsController` class `getSearchAction()` function. The global `$_GET`
variable will have the following data.

    [
        'query' => 'Hello World'
    ]

You can also use the method `self::getPostData()` and it will return the POST data automatically
(whether it was posted through JSON or URL query parameters).

### URL Rewrite

Besides that, you can also use the URL rewrite method (not recommended for long queries).
Declare your function with parameters and you're good to go. This works for any HTTP method.

    <?php
    final class PostsController extends \PhpWebApi\Controller {
        // GET http://api.example.com/posts/data/id/{id}
        function getDataAction($params) {
            if (!isset($params['id'])) {
                self::badRequest('Invalid request');
            }

            self::text('I have received the params with post id ' . $params['id']);
        }
    }

You can also pass multiple parameters.

    <?php
    // GET http://api.example.com/comments/data/postId/{id}/userId/{id}
    final class CommentsController extends \PhpWebApi\Controller {
        function getDataAction($params) {
            if (!isset($params['postId']) || !isset($params['userId']) {
                self::badRequest('Invalid request');
            }

            self::text('I have received the params with post id ' . $params['postId']
                . ' and user id ' . $params['userId']);
        }
    }

## Returning Views

You can create view files (.phtml) to render HTML responses. Simply create a folder with the same name as
the controller under the `views` folder. For example:

    <?php
    // File: controllers/HomeController.php
    final class HomeController extends \PhpWebApi\Controller {
        public function getIndexAction() {
            self::view();
        }
    }


    <!-- File: views/Home/Index.phtml -->
    <h1>Hello World!</h1>

    <p>Welcome to PHP Web Deploy, a light-weight alternative PHP MVC Framework for small projects.</p>

Within the view file, you can call `self::baseHref()` or `self::href('/path/to/child')` and it will
echo the app base href (ex: http://example.com/yourappname) and the href (http://example.com/yourappname/path/to/child)
accordingly.

    <p>Click <a href="<?php self::href('/account/signin'); ?>">here</a> to sign in.</p>


## Passing data to views

You can also pass data to the view file. The view data can be any variable type.

    <?php
    // File: controllers/HomeController.php
    final class HomeController extends \PhpWebApi\Controller {
        public function getIndexAction() {
            self::view('Hello World');
        }
    }


    <!-- File: views/Home/Index.phtml -->
    <h1><?php echo $viewData; ?></h1>

    <p>Welcome to PHP Web Deploy, a light-weight alternative PHP MVC Framework for small projects.</p>

## Shared Layout

Shared layout allows you to reuse a specific layout for your responses.
By default, this will look for `Shared/Layout.phtml`. If no shared layout is found, the view file is returned.

    <!-- File: views/Shared/Layout.phtml -->
    <!doctype html>
    <html lang="en">
        <head>
            <title>Your App Name</title>
        </head>
        <body>

            <?php // This renders the response view file ?>
            <?php self::render('body'); ?>

        </body>
    </html>

If you do not want to include the layout, or use a different layout,
you can pass the view name as the second parameter.

    <?php
    self::view($viewData, false); // false for no shared layout

    self::view($viewData, 'Custom'); // includes views/Shared/Custom.phtml

## Static files (or assets)

Create a folder `assets` and place your static files (such as images) here.
