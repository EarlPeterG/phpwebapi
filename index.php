<?php
define('APP_ROOT', __DIR__);
define('APP_BASE_HREF', '/');
require_once 'vendor/autoload.php';


\PhpWebApi\Router::getInstance()
	->allowCors('*', 'Authorization', 'DELETE, GET, OPTIONS, POST, PUT')
	->bootstrap();
