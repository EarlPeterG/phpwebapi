<?php
namespace PhpWebApi;

class Router {
	private static $_instance;
	private $controllerName;
	private $actionName;

	// Preflight cors headers
	private $allowCors = false;
	private $allowAuthentication = false;
	private $allowedHeaders = '';
	private $allowedOrigin = '';
	private $allowedMethods = '';
	private $maxAge = 600;
	private $additionalHeaders = [];

	public function __construct () { self::$_instance = $this; }

	/**
	 * Gets the static instance of Router.
	 */
	public static function getInstance() {
        return empty(self::$_instance) ? new self : self::$_instance;
    }

	/**
	 * Returns the controller name of the current request.
	 * @return string
	 */
	public function getControllerName() {
		return self::getInstance()->controllerName;
	}

	/**
	 * Returns the action name of the current request.
	 * @return string
	 */
	public function getActionName() {
		return self::getInstance()->actionName;
	}

	/**
	 * Returns the base href of the current request.
	 * @return string
	 */
	public function getBaseHref() {
		return "http" . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "s" : "")
			. "://$_SERVER[HTTP_HOST]" . APP_BASE_HREF;
	}

	/**
	 * Send no-cache headers.
	 * @param bool $noCacheHeaders
	 */
	public function setNoCacheHeaders($noCacheHeaders) {
		$this->setAdditionalHeader('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0');
		$this->setAdditionalHeader('Pragma', 'no-cache');
		return $this;
	}

	/**
	 * Allows Cross-Origin Resource sharing.
	 * @param string $allowedOrigin
	 * @param string $allowedHeaders
	 * @param string $allowedMethods
	 * @param bool $allowCredentials
	 * @param int $maxAge
	 */
	public function allowCors($allowedOrigin = '*', $allowedHeaders = '*', $allowedMethods = 'DELETE, GET, POST, PUT', $allowCredentials = true, $maxAge = 600) {
		$this->allowCors = true;
		$this->allowedHeaders = $allowedHeaders;
		$this->allowedOrigin = $allowedOrigin;
		$this->allowedMethods = $allowedMethods;
		$this->allowCredentials = $allowCredentials;
		$this->maxAge = $maxAge;
		return $this;
	}

	/**
	 * Set an additional header.
	 * @param string $key
	 * @param string $val
	 */
	public function setAdditionalHeader($key, $value) {
		$this->additionalHeaders[$key] = $value;
		return $this;
	}

	/**
	 * Set the additional headers array.
	 * @param array $additionalHeaders
	 */
	public function setAdditionalHeaders($additionalHeaders) {
		$this->additionalHeaders = $additionalHeaders;
		return $this;
	}


	/**
	 * Begin sending headers and calling the request
	 */
	public function bootstrap() {
		$requestMethod = strtolower($_SERVER['REQUEST_METHOD']);

		if ($this->allowCors && $requestMethod === 'options') { $this->sendPreflightResponse(); }

		$this->sendHeaders();
		$urlParams = $this->getUrlRequest();

		$controllerParam = array_shift($urlParams);
		if (empty($controllerParam)) { $controllerParam = 'Home'; }
		if (!$this->isValidName($controllerParam)) { Controller::notFound(); }

		$this->controllerName = $controllerName = $this->dashesToCamelCase($controllerParam);
		$fileName = APP_ROOT.'/controllers/'.$controllerName.'Controller.php';
		if (!file_exists($fileName)) { Controller::notFound(); }


		$requestParam = array_shift($urlParams);
		if (empty($requestParam)) { $requestParam = 'Index'; }
		if (!$this->isValidName($requestParam)) { Controller::notFound(); }


		$urlParams = $this->parseUrlParams($urlParams);


		// Call the action
		include_once $fileName;
		$controller = $controllerName.'Controller';
		$controller = new $controller;

		$this->actionName = $this->dashesToCamelCase($requestParam);
		$actionName = $requestMethod.$this->actionName.'Action';
		if (!in_array($actionName, get_class_methods($controller))) { Controller::notFound(); }

		try {
			if (empty($urlParams)) {
				$controller->$actionName();
			} else {
				$controller->$actionName($urlParams);
			}
		} catch(\InvalidArgumentException $ex) {
			Controller::badRequest($ex->getMessage());
		}
	}


	// -----------------
	// Private Functions

	private function dashesToCamelCase($string, $capitalizeFirstCharacter = true) {
		$str = str_replace('-', '', ucwords($string, '-'));
		if (!$capitalizeFirstCharacter) { $str = lcfirst($str); }

		return $str;
	}

	private function getUrlRequest() {
		$requestString = empty($_SERVER['REQUEST_URI']) ?
			$_SERVER['REDIRECT_URL'] : $_SERVER['REQUEST_URI'];

		if(strpos($requestString, '?') !== FALSE) {
			$requestString = substr($requestString, 0, strpos($requestString, '?'));
		}

		if (defined('APP_BASE_HREF')
			// check if string starts with APP_BASE_HREF
			&& (strcasecmp(substr($requestString, 0, strlen(APP_BASE_HREF)), APP_BASE_HREF) === 0)) {
			$requestString = substr($requestString, strlen(APP_BASE_HREF));
		}

		return explode('/', $requestString);
	}

	private function isValidName($varname) {
		return (bool)preg_match('/^[\w-]+$/i', $varname);
	}

	private function parseUrlParams($urlParams) {
		$aParams = [];
		if (current($urlParams)) {
			for($i=0; $i < count($urlParams); $i+=2) {
				$aParams[$urlParams[$i]] = isset($urlParams[$i+1]) ? $urlParams[$i+1] : null;
			}
		}

		return $aParams;
	}

	private function sendHeaders() {
		if ($this->allowCors && !empty($this->allowedOrigin)) {
			header('Access-Control-Allow-Origin: '.$this->allowedOrigin);
		}

		foreach($this->additionalHeaders as $header => $value) { header("$header: $value"); }
	}

	private function sendPreflightResponse() {
		if (!empty($this->allowedHeaders)) { header('Access-Control-Allow-Headers: '.$this->allowedHeaders); }
		if (!empty($this->allowedMethods)) { header('Access-Control-Allow-Methods: '.$this->allowedMethods); }
		if (!empty($this->allowedOrigin)) { header('Access-Control-Allow-Origin: '.$this->allowedOrigin); }
		if ($this->allowCredentials) { header('Access-Control-Allow-Credentials: true'); }
		if (!empty($this->maxAge)) { header('Access-Control-Max-Age: '.$this->maxAge); }

		exit(0); // HTTP OK
	}
}
