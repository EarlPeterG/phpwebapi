<?php
namespace PhpWebApi;

abstract class Controller {
    /**
     * Returns the current POST data (whether JSON or url encoded)
     * @return mixed
     */
    public static function getPostData() {
        return getallheaders()['Content-Type'] == 'application/json' ?
            json_decode(file_get_contents('php://input'), true) : $_POST;
    }

	/**
	 * Returns a bad request response (400) and ends the script.
	 * @param mixed $data
	 */
	public static function badRequest($data = null) {
		http_response_code(400);
		self::json($data);
	}

	/**
	 * Returns a not found response (404) and ends the script.
	 * @param mixed $data
	 */
	public static function notFound($data = null) {
		http_response_code(404);
		self::json($data);
	}

	/**
	 * Returns an ok response (200) and ends the script.
	 * @param mixed $data
	 */
	public static function ok($data = null) {
		http_response_code(200);
		self::json($data);
	}

	/**
	 * Returns an unauthorized response (401) and ends the script.
	 * @param mixed $data
	 */
	public static function unauthorized($data = null) {
		http_response_code(401);
		self::json($data);
	}

	/**
	 * Returns a server error response (500) and ends the script.
	 * @param mixed $data
	 */
	public static function serverError($data = null) {
		http_response_code(500);
		self::json($data);
	}


	/**
	 * Prints out data in json format and ends the script.
	 * @param $model nested array of data
	 */
	public static function csv($filename, $model) {
		if (is_numeric($model) || is_string($model) || is_bool($model)) {
			self::text($model);
		}
		else if (!empty($model)) {
			header('Content-Type: text/csv');
			header("Content-Disposition: attachment; filename=$filename");
			$file = fopen('php://output', 'w');
			foreach($model as $row) {
				fputcsv($file, $row);
			}
		}

		exit();
	}

	/**
	 * Prints out data in json format and ends the script.
	 */
	public static function json($model) {
		if (is_numeric($model) || is_string($model) || is_bool($model)) {
			self::text($model);
		}
		else if (!empty($model)) {
			header('Content-Type: application/json');
			echo json_encode($model);
		}

		exit();
	}

	/**
	 * Prints out data in text format and ends the script.
	 */
	public static function text($model) {
		header('Content-Type: text/plain');
		if (is_bool($model)) {
			echo $model ? 'true' : 'false';
		} else {
			echo $model;
		}
		exit();
	}

	/**
	 * Redirects a user to a controller and/or action and ends the script.
	 */
	public static function redirect($controller, $action = '', $params = []) {
		$url = Router::getInstance()->getBaseHref()."$controller/";
		if (!empty($action)) {
			$url .= "$action";
		}

		if (!empty($params)) {
			if (empty($action)) {
				$url .= "index";
			}

			foreach($params as $k => $v) {
				$key = urlencode($k);
				$value = urlencode($v);
				$url .= "/$key/$value";
			}
		}

		header("Location: $url");
		exit();
	}

	/**
	 * Redirects a user to the current controller's action and ends the script.
	 */
	public static function redirectToAction($action, $params = []) {
		self::redirect(Router::getInstance()->getControllerName(), $action, $params);
	}

	/**
	 * Redirects a user to an external URL and ends the script.
	 */
	public static function redirectToExternal($url) {
		if (filter_var($url, FILTER_VALIDATE_URL)) { header("Location: $url"); }
		exit();
	}

	/**
	 * Locates the current request's view file, includes it and ends the script.
	 * @param mixed $viewData
	 * @param string|boolean $includeSharedLayout
	 */
	public static function view($viewData = null, $includeSharedLayout = 'Layout') {
		new View($viewData, $includeSharedLayout);
	}
}
