<?php
namespace PhpWebApi;

class View {
	private static $renderData = [];

	public function __construct($viewData = null, $sharedLayoutFile) {
		$viewFile = APP_ROOT.'/views/' . strtolower(Router::getInstance()->getControllerName()) .
			'/'. strtolower(Router::getInstance()->getActionName()).'.phtml';

		if (file_exists($viewFile)) {
			if (!empty($sharedLayoutFile)) {
				$sharedLayoutFile = APP_ROOT.'/views/Shared/'.$sharedLayoutFile.'.phtml';

				if (file_exists($sharedLayoutFile)) {
					ob_start();
					include $viewFile;
					self::$renderData['body'] = ob_get_contents();
					ob_end_clean();

					include $sharedLayoutFile;
					return;
				}
			}

			include $viewFile;
			return;
		} else {
			Controller::notFound();
		}
	}


	public static function baseHref() {
		echo Router::getInstance()->getBaseHref();
	}

	public static function href($url) {
		echo Router::getInstance()->getBaseHref() . ltrim($url, '/');
	}

	public static function render($key) {
		if (isset(self::$renderData[$key])) { echo self::$renderData[$key]; }
	}
}
